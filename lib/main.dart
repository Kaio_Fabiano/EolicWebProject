import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'dart:math';

import 'package:flutter/foundation.dart';

import 'package:web_aplication/responsive.dart';
import 'package:web_aplication/telas/tela_mobile.dart';

import 'telas/tela_inicial.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> with TickerProviderStateMixin {
  //Declaracao de variaveis

  late Animation<double> rotateAnimation;

  late AnimationController rotateAnimController;
  late AnimationController _fadeController;
  late Animation<double> _fadeAnimation;
  ScrollController _scrollController = new ScrollController();
  var _backgroundColor;
  var widthSize;
  var heighSize;

  double headerHeighSize = 0;
  double article1HeighSize = 0;
  List<double> article2HeighSizes = [];

  var footerHeighSize = 0;

  AnimationController defineAnimationCurved1() {
    return AnimationController(
      duration: const Duration(seconds: 2),
      vsync: this,
    );
  }

  AnimationController defineAnimationCurved2() {
    return AnimationController(
      duration: const Duration(seconds: 2),
      vsync: this,
    );
  }

  AnimationController defineAnimationCurved3() {
    return AnimationController(
      duration: const Duration(seconds: 2),
      vsync: this,
    );
  }

  AnimationController defineAnimationCurved4() {
    return AnimationController(
      duration: const Duration(seconds: 2),
      vsync: this,
    );
  }

  AnimationController defineAnimationCurved5() {
    return AnimationController(
      duration: const Duration(seconds: 2),
      vsync: this,
    );
  }

  AnimationController defineAnimationCurved6() {
    return AnimationController(
      duration: const Duration(seconds: 2),
      vsync: this,
    );
  }

  late final AnimationController _leftController1 = defineAnimationCurved1();
  late final AnimationController _rightController1 = defineAnimationCurved1();
  late final AnimationController _leftController2 = defineAnimationCurved2();
  late final AnimationController _rightController2 = defineAnimationCurved2();
  late final AnimationController _leftController3 = defineAnimationCurved3();
  late final AnimationController _rightController3 = defineAnimationCurved3();
  late final AnimationController _leftController4 = defineAnimationCurved4();
  late final AnimationController _rightController4 = defineAnimationCurved4();
  late final AnimationController _leftController5 = defineAnimationCurved5();
  late final AnimationController _rightController5 = defineAnimationCurved5();
  late final AnimationController _leftController6 = defineAnimationCurved6();
  late final AnimationController _rightController6 = defineAnimationCurved6();

  late List<Animation<Offset>> animationsRight = [];
  late List<Animation<Offset>> animationsLeft = [];

  late final Animation<Offset> _rightOffsetAnimation1 =
      configAnimationCurvedRight().animate(CurvedAnimation(
    parent: _rightController1,
    curve: Curves.linearToEaseOut,
  ));

  late final Animation<Offset> __leftOffsetAnimation1 =
      configAnimationCurvedLeft().animate(CurvedAnimation(
    parent: _leftController1,
    curve: Curves.linearToEaseOut,
  ));
  late final Animation<Offset> _rightOffsetAnimation2 =
      configAnimationCurvedRight().animate(CurvedAnimation(
    parent: _rightController2,
    curve: Curves.linearToEaseOut,
  ));

  late final Animation<Offset> __leftOffsetAnimation2 =
      configAnimationCurvedLeft().animate(CurvedAnimation(
    parent: _leftController2,
    curve: Curves.linearToEaseOut,
  ));
  late final Animation<Offset> _rightOffsetAnimation3 =
      configAnimationCurvedRight().animate(CurvedAnimation(
    parent: _rightController3,
    curve: Curves.linearToEaseOut,
  ));

  late final Animation<Offset> __leftOffsetAnimation3 =
      configAnimationCurvedLeft().animate(CurvedAnimation(
    parent: _leftController3,
    curve: Curves.linearToEaseOut,
  ));
  late final Animation<Offset> _rightOffsetAnimation4 =
      configAnimationCurvedRight().animate(CurvedAnimation(
    parent: _rightController4,
    curve: Curves.linearToEaseOut,
  ));

  late final Animation<Offset> __leftOffsetAnimation4 =
      configAnimationCurvedLeft().animate(CurvedAnimation(
    parent: _leftController4,
    curve: Curves.linearToEaseOut,
  ));

  late final Animation<Offset> _rightOffsetAnimation5 =
      configAnimationCurvedRight().animate(CurvedAnimation(
    parent: _rightController5,
    curve: Curves.linearToEaseOut,
  ));

  late final Animation<Offset> __leftOffsetAnimation5 =
      configAnimationCurvedLeft().animate(CurvedAnimation(
    parent: _leftController5,
    curve: Curves.linearToEaseOut,
  ));

  late final Animation<Offset> _rightOffsetAnimation6 =
      configAnimationCurvedRight().animate(CurvedAnimation(
    parent: _rightController6,
    curve: Curves.linearToEaseOut,
  ));

  late final Animation<Offset> __leftOffsetAnimation6 =
      configAnimationCurvedLeft().animate(CurvedAnimation(
    parent: _leftController6,
    curve: Curves.linearToEaseOut,
  ));

  Tween<Offset> configAnimationCurvedLeft() {
    return Tween<Offset>(
      begin: const Offset(-1.0, 0.0),
      end: const Offset(0.0, 0.0),
    );
  }

  Tween<Offset> configAnimationCurvedRight() {
    return Tween<Offset>(
      begin: const Offset(1.0, 0.0),
      end: const Offset(0.0, 0.0),
    );
  }

  // Init State
  @override
  void initState() {
    super.initState();
    rotateAnimController = AnimationController(
      duration: Duration(seconds: 5),
      vsync: this,
    );
    _scrollController.addListener(onScroll);

    rotateAnimation = Tween<double>(
      begin: 0,
      end: 2 * pi,
    ).animate(rotateAnimController)
      ..addListener(() {
        // Empty setState because the updated value is already in the animation field
        setState(() {});
      })
      ..addStatusListener((status) {
        if (status == AnimationStatus.completed) {
          rotateAnimController.repeat();
        } else if (status == AnimationStatus.dismissed) {
          rotateAnimController.forward();
        }
      });

    rotateAnimController.forward();

    _fadeController = AnimationController(
      duration: const Duration(seconds: 3),
      vsync: this,
    );
    _fadeAnimation =
        CurvedAnimation(parent: _fadeController, curve: Curves.easeIn);
    animationsLeft.add(__leftOffsetAnimation1);
    animationsLeft.add(__leftOffsetAnimation2);
    animationsLeft.add(__leftOffsetAnimation3);
    animationsLeft.add(__leftOffsetAnimation4);
    animationsLeft.add(__leftOffsetAnimation5);
    animationsLeft.add(__leftOffsetAnimation6);
    animationsRight.add(_rightOffsetAnimation1);
    animationsRight.add(_rightOffsetAnimation2);
    animationsRight.add(_rightOffsetAnimation3);
    animationsRight.add(_rightOffsetAnimation4);
    animationsRight.add(_rightOffsetAnimation5);
    animationsRight.add(_rightOffsetAnimation6);

    double sizeArticle1 = 0;
    double sizeArticle2 = 0;
    double sizeArticle3 = 0;
    double sizeArticle4 = 0;
    double sizeArticle5 = 0;
    double sizeArticle6 = 0;
    article2HeighSizes.add(sizeArticle1);
    article2HeighSizes.add(sizeArticle2);
    article2HeighSizes.add(sizeArticle3);
    article2HeighSizes.add(sizeArticle4);
    article2HeighSizes.add(sizeArticle5);
    article2HeighSizes.add(sizeArticle6);
  }

  onScroll() {
    if ((headerHeighSize.toDouble() * .7) <= _scrollController.offset) {
      _fadeController..forward();
    }

    if ((headerHeighSize.toDouble() + article1HeighSize.toDouble()) <=
        _scrollController.offset) {
      _rightController1.forward();
      _leftController1.forward();
    }
    if ((headerHeighSize.toDouble() +
            (article1HeighSize.toDouble()) +
            (article2HeighSizes[0] * .2)) <=
        _scrollController.offset) {
      _rightController2.forward();
      _leftController2.forward();
    }
    if ((headerHeighSize.toDouble() +
            article1HeighSize.toDouble() +
            (article2HeighSizes[0] * .35)) <=
        _scrollController.offset) {
      _rightController3.forward();
      _leftController3.forward();
    }
    if ((headerHeighSize.toDouble() +
            article1HeighSize.toDouble() +
            (article2HeighSizes[0] * .45)) <=
        _scrollController.offset) {
      _rightController4.forward();
      _leftController4.forward();
    }
    if ((headerHeighSize.toDouble() +
            article1HeighSize.toDouble() +
            (article2HeighSizes[0] * .55)) <=
        _scrollController.offset) {
      _rightController5.forward();
      _leftController5.forward();
    }
    if ((headerHeighSize.toDouble() +
            article1HeighSize.toDouble() +
            (article2HeighSizes[0] * .65)) <=
        _scrollController.offset) {
      _rightController6.forward();
      _leftController6.forward();
    }
  }

  String path(str) {
    return (kIsWeb) ? 'assets/$str' : str;
  }

  @override
  void dispose() {
    rotateAnimController.dispose();
    _fadeController.dispose();
    _leftController1.dispose();
    _rightController1.dispose();
    _leftController2.dispose();
    _rightController2.dispose();
    _leftController3.dispose();
    _rightController3.dispose();
    _leftController4.dispose();
    _rightController4.dispose();
    _leftController5.dispose();
    _rightController5.dispose();
    _leftController6.dispose();
    _rightController6.dispose();
    super.dispose();
  }

  //Scaffold
  @override
  Widget build(BuildContext context) {
    widthSize = MediaQuery.of(context).size.width;
    heighSize = MediaQuery.of(context).size.height;
    headerHeighSize = widthSize * .400;
    article1HeighSize = widthSize * .600;
    double bemVindoSize = widthSize * .04;
    article2HeighSizes[0] = widthSize * .35;
    article2HeighSizes[1] = widthSize * .35;
    article2HeighSizes[2] = widthSize * .35;
    article2HeighSizes[3] = widthSize * .35;
    article2HeighSizes[4] = widthSize * .35;
    article2HeighSizes[5] = widthSize * .35;

    _backgroundColor = Colors.white;

    return Scaffold(
      body: Responsive(
        desktop: buildBody(
          _backgroundColor,
          context,
          rotateAnimation,
          _fadeAnimation,
          widthSize,
          heighSize,
          headerHeighSize,
          _scrollController,
          animationsLeft,
          animationsRight,
          article1HeighSize,
          article2HeighSizes,
          bemVindoSize,
          path,
        ),
        mobile: buildMobileBody(
          _backgroundColor,
          context,
          rotateAnimation,
          _fadeAnimation,
          widthSize,
          heighSize,
          headerHeighSize,
          _scrollController,
          animationsLeft,
          animationsRight,
          article1HeighSize,
          article2HeighSizes,
          bemVindoSize,
          path,
        ),
      ),
    );
  }
}

//Body

// Sections

//  Widgets








