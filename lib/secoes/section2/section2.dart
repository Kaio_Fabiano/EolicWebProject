import 'package:flutter/material.dart';

import 'articulos/articulo1.dart';
import 'articulos/articulo2.dart';
import 'articulos/articulo3.dart';
import 'articulos/articulo4.dart';
import 'articulos/articulo5.dart';
import 'articulos/articulo6.dart';

buildArticle2(
  List<Animation<Offset>> _leftOffsetAnimation,
  List<Animation<Offset>> _rightOffsetAnimation,
  widthSize,
  heighSize,
  article2HeighSizes,
  path,
) {
  return Container(
    child: Column(
      children: [
        Text(
          "NOSSOS SERVIÇOS",
          style: TextStyle(
            color: Color(0xff06C8BE),
            fontSize: widthSize * .036,
            fontWeight: FontWeight.bold,
            fontFamily: "MazzardH",
          ),
        ),
        SizedBox(
          height: 50,
        ),
        article1(
          _leftOffsetAnimation[0],
          _rightOffsetAnimation[0],
          widthSize,
          article2HeighSizes[0],
          path,
        ),
        article2(
          _leftOffsetAnimation[1],
          _rightOffsetAnimation[1],
          widthSize,
          article2HeighSizes[1],
          path,
        ),
        article3(
          _leftOffsetAnimation.elementAt(2),
          _rightOffsetAnimation.elementAt(2),
          widthSize,
          article2HeighSizes[2],
          path,
        ),
        article4(
          _leftOffsetAnimation.elementAt(3),
          _rightOffsetAnimation.elementAt(3),
          widthSize,
          article2HeighSizes[3],
          path,
        ),
        article5(
          _leftOffsetAnimation.elementAt(4),
          _rightOffsetAnimation.elementAt(4),
          widthSize,
          article2HeighSizes[4],
          path,
        ),
        article6(
          _leftOffsetAnimation.elementAt(5),
          _rightOffsetAnimation.elementAt(5),
          widthSize,
          article2HeighSizes[5],
          path,
        ),
      ],
    ),
  );
}
