import 'package:flutter/material.dart';

import 'package:web_aplication/widgets/mobile/mobileBuildTextArt2.dart';
import 'package:web_aplication/widgets/mobile/mobileBuildTitleTextArt2.dart';

Container article3Mobile(
  widthSize,
  heighSize,
  path,
) {
  return Container(
    height: heighSize,
    child: Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Expanded(
          flex: 5,
          child: Container(
            alignment: Alignment.centerLeft,
            child: Container(
              height: widthSize * .5,
              width: widthSize * .5,
              child: Image.asset(path("Img/Imagem-03.png")),
            ),
          ),
        ),
        Container(
          child: Expanded(
            flex: 5,
            child: Padding(
              padding: const EdgeInsets.all(30.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  buildTitleTextArt2Mobile(
                    path,
                    "INSPEÇÃO DE RECEBIMENTO DE PÁS EM CAMPO.",
                    widthSize,
                    TextAlign.right,
                  ),
                  buildTextArt2Mobile(
                    path,
                    "INSPEÇÃO VISUAL EXTERNA/INTERNA ANTES DO DESCARREGAMENTO DAS PÁS.\n\n",
                    widthSize,
                    TextAlign.right,
                  ),
                  buildTextArt2Mobile(
                    path,
                    "INSPEÇÃO PRÉ-MONTAGEM/INSPEÇÃO VISUAL DETALHADA COM EXPEDIÇÃO DE RELATÓRIOS TÉCNICOS.",
                    widthSize,
                    TextAlign.right,
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    ),
  );
}
