import 'package:flutter/material.dart';

import 'package:web_aplication/widgets/mobile/mobileBuildTextArt2.dart';
import 'package:web_aplication/widgets/mobile/mobileBuildTitleTextArt2.dart';

Container article2Mobile(
  widthSize,
  heighSize,
  path,
) {
  return Container(
    height: heighSize,
    child: Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
          child: Expanded(
            flex: 5,
            child: Padding(
              padding: const EdgeInsets.all(30.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  buildTitleTextArt2Mobile(
                    path,
                    "ACOMPANHAMENTO E MONITORAMENTO DE EXPEDIÇÃO.",
                    widthSize,
                    TextAlign.left,
                  ),
                  buildTextArt2Mobile(
                    path,
                    "INSPEÇÃO VISUAL EXTERNA DA SUPERFÍCIE.\n",
                    widthSize,
                    TextAlign.left,
                  ),
                  buildTextArt2Mobile(
                    path,
                    "INSPEÇÃO DURANTE EMBARQUES EM PORTOS E CARREGAMENTO EM BASES",
                    widthSize,
                    TextAlign.left,
                  ),
                ],
              ),
            ),
          ),
        ),
        Expanded(
          flex: 5,
          child: Container(
            alignment: Alignment.centerRight,
            child: Container(
              height: widthSize * .5,
              width: widthSize * .5,
              child: Image.asset(path("Img/Imagem-02.png")),
            ),
          ),
        ),
      ],
    ),
  );
}
