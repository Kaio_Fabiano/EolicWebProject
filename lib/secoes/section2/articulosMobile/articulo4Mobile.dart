import 'package:flutter/material.dart';

import 'package:web_aplication/widgets/mobile/mobileBuildTextArt2.dart';
import 'package:web_aplication/widgets/mobile/mobileBuildTitleTextArt2.dart';

Container article4Mobile(
  widthSize,
  heighSize,
  path,
) {
  return Container(
    child: Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
          child: Expanded(
            flex: 5,
            child: Padding(
              padding: const EdgeInsets.all(30.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  buildTitleTextArt2Mobile(
                    path,
                    "INSPEÇÃO PÓS MONTAGEM.",
                    widthSize,
                    TextAlign.right,
                  ),
                  buildTextArt2Mobile(
                    path,
                    "INSPEÇÃO VISUAL AUXILIO DE DRONES.\n",
                    widthSize,
                    TextAlign.right,
                  ),
                  buildTextArt2Mobile(
                    path,
                    "INSPEÇÃO VISUAL COM AUXILIO DE CORDAS.",
                    widthSize,
                    TextAlign.right,
                  ),
                ],
              ),
            ),
          ),
        ),
        Expanded(
          flex: 5,
          child: Container(
            alignment: Alignment.centerLeft,
            child: Container(
              height: widthSize * .5,
              width: widthSize * .5,
              child: Image.asset(path("Img/ImagemD-04.png")),
            ),
          ),
        ),
      ],
    ),
  );
}
