import 'package:flutter/material.dart';
import 'package:web_aplication/widgets/mobile/mobileBuildTextArt2.dart';
import 'package:web_aplication/widgets/mobile/mobileBuildTitleTextArt2.dart';

Container article5Mobile(
  widthSize,
  heighSize,
  path,
) {
  return Container(
    child: Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Expanded(
          flex: 6,
          child: Container(
            alignment: Alignment.centerLeft,
            child: Container(
              height: widthSize * .5,
              width: widthSize * .5,
              child: Image.asset(path("Img/ImagemD-05.png")),
            ),
          ),
        ),
        Container(
          child: Expanded(
            flex: 5,
            child: Padding(
              padding: const EdgeInsets.all(30.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  buildTitleTextArt2Mobile(
                    path,
                    "INSPEÇÃO PREVENTIVAS",
                    widthSize,
                    TextAlign.right,
                  ),
                  buildTextArt2Mobile(
                    path,
                    "INSPEÇÃO SISTEMA DE PARA RAIO.\n",
                    widthSize,
                    TextAlign.right,
                  ),
                  buildTextArt2Mobile(
                    path,
                    "INSPEÇÃO DE SUPERFÍCIE ESTRUTURAL E PINTURA.",
                    widthSize,
                    TextAlign.right,
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    ),
  );
}
