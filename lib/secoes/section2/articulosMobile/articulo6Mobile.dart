import 'package:flutter/material.dart';
import 'package:web_aplication/widgets/mobile/mobileBuildTextArt2.dart';
import 'package:web_aplication/widgets/mobile/mobileBuildTitleTextArt2.dart';

Container article6Mobile(
  widthSize,
  heighSize,
  path,
) {
  return Container(
    child: Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
          child: Expanded(
            flex: 5,
            child: Padding(
              padding: const EdgeInsets.all(30.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  buildTitleTextArt2Mobile(
                    path,
                    "INSPEÇÃO FINAL DE GARANTIA.",
                    widthSize,
                    TextAlign.left,
                  ),
                  buildTextArt2Mobile(
                    path,
                    "MAPEAMENTO DOS CRITÉRIOS DE QUALIDADE ESTABELECIDOS EM CONTRATOS.",
                    widthSize,
                    TextAlign.left,
                  ),
                ],
              ),
            ),
          ),
        ),
        Expanded(
          flex: 6,
          child: Container(
            alignment: Alignment.centerRight,
            child: Container(
              height: widthSize * .5,
              width: widthSize * .5,
              child: Image.asset(path("Img/ImagemDN-06.png")),
            ),
          ),
        ),
      ],
    ),
  );
}
