import 'package:flutter/cupertino.dart';

import 'package:web_aplication/widgets/mobile/mobileBuildTextArt2.dart';
import 'package:web_aplication/widgets/mobile/mobileBuildTitleTextArt2.dart';

Container article1Mobile(
  widthSize,
  heighSize,
  path,
) {
  return Container(
    height: heighSize,
    child: Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Expanded(
          flex: 5,
          child: Container(
            alignment: Alignment.centerLeft,
            child: Container(
              height: widthSize * .5,
              width: widthSize * .5,
              child: Image.asset(path("Img/Imagem-01.png")),
            ),
          ),
        ),
        Expanded(
          flex: 7,
          child: Padding(
            padding: const EdgeInsets.all(30.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                buildTitleTextArt2Mobile(
                  path,
                  "INSPEÇÃO DE PÁS\.",
                  widthSize * 1.6,
                  TextAlign.right,
                ),
                buildTextArt2Mobile(
                  path,
                  "AUDITORIA E INSPEÇÃO PARA VALIDAÇÃO DE PÁS NA BASE DE FABRICAÇÃO/\nACOMPANHAMENTO DO PROCESSO DE FABRICAÇÃO COMPLETO.\n\n",
                  widthSize * 1.6,
                  TextAlign.right,
                ),
                buildTextArt2Mobile(
                  path,
                  "AUDITORIA E INSPEÇÃO PARA ACOMPANHAMENTO E EXECUÇÃO DE REPAROS E RETRABALHOS COMPLEXOS.\n",
                  widthSize * 1.6,
                  TextAlign.right,
                ),
                buildTextArt2Mobile(
                  path,
                  "AUDITORIA E INSPEÇÃO EM PROCESSOS DE ULTRASOM.\n",
                  widthSize * 1.6,
                  TextAlign.right,
                ),
                buildTextArt2Mobile(
                  path,
                  "AUDITORIA E INSPEÇÃO EM PROCESSOS DE ACABAMENTO E LIBERAÇÃO.\n",
                  widthSize * 1.6,
                  TextAlign.right,
                ),
                buildTextArt2Mobile(
                  path,
                  "INSPEÇÃO INTERNA COM AUXILIO DE HOVER CÂMERA",
                  widthSize * 1.6,
                  TextAlign.right,
                ),
              ],
            ),
          ),
        ),
      ],
    ),
  );
}
