import 'package:flutter/material.dart';
import 'package:web_aplication/widgets/desktop/buildTextArt2.dart';
import 'package:web_aplication/widgets/desktop/buildTitleTextArt2.dart';

Container article3(
  _leftOffsetAnimation,
  _rightOffsetAnimation,
  widthSize,
  heighSize,
  path,
) {
  return Container(
    height: heighSize,
    child: Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Expanded(
          flex: 3,
          child: SlideTransition(
            position: _leftOffsetAnimation,
            child: Container(
              alignment: Alignment.centerLeft,
              child: FractionallySizedBox(
                widthFactor: 1,
                child: Image.asset(path("Img/Imagem-03.png")),
              ),
            ),
          ),
        ),
        Container(
          child: Expanded(
            flex: 7,
            child: SlideTransition(
              position: _rightOffsetAnimation,
              child: Padding(
                padding: const EdgeInsets.all(30.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    buildTitleTextArt2(
                      path,
                      "INSPEÇÃO DE RECEBIMENTO DE PÁS EM CAMPO.",
                      widthSize,
                      TextAlign.right,
                    ),
                    buildTextArt2(
                      path,
                      "INSPEÇÃO VISUAL EXTERNA/INTERNA ANTES DO DESCARREGAMENTO DAS PÁS.",
                      widthSize,
                      TextAlign.right,
                    ),
                    buildTextArt2(
                      path,
                      "INSPEÇÃO PRÉ-MONTAGEM/INSPEÇÃO VISUAL DETALHADA COM EXPEDIÇÃO DE RELATÓRIOS TÉCNICOS.",
                      widthSize,
                      TextAlign.right,
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ],
    ),
  );
}
