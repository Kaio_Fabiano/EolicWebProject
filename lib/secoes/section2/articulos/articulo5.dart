import 'package:flutter/material.dart';
import 'package:web_aplication/widgets/desktop/buildTextArt2.dart';
import 'package:web_aplication/widgets/desktop/buildTitleTextArt2.dart';

Container article5(
  _leftOffsetAnimation,
  _rightOffsetAnimation,
  widthSize,
  heighSize,
  path,
) {
  return Container(
    height: heighSize,
    child: Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Expanded(
          flex: 6,
          child: SlideTransition(
            position: _leftOffsetAnimation,
            child: Container(
              alignment: Alignment.centerLeft,
              child: FractionallySizedBox(
                widthFactor: 1,
                child: Image.asset(path("Img/ImagemD-05.png")),
              ),
            ),
          ),
        ),
        Container(
          child: Expanded(
            flex: 5,
            child: SlideTransition(
              position: _rightOffsetAnimation,
              child: Padding(
                padding: const EdgeInsets.all(30.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    buildTitleTextArt2(
                      path,
                      "INSPEÇÃO PREVENTIVAS",
                      widthSize,
                      TextAlign.right,
                    ),
                    buildTextArt2(
                      path,
                      "INSPEÇÃO SISTEMA DE PARA RAIO.",
                      widthSize,
                      TextAlign.right,
                    ),
                    buildTextArt2(
                      path,
                      "INSPEÇÃO DE SUPERFÍCIE ESTRUTURAL E PINTURA.",
                      widthSize,
                      TextAlign.right,
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ],
    ),
  );
}
