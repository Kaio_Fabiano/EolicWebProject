import 'package:flutter/material.dart';
import 'package:web_aplication/widgets/desktop/buildTextArt2.dart';
import 'package:web_aplication/widgets/desktop/buildTitleTextArt2.dart';

Container article2(
  _leftOffsetAnimation,
  _rightOffsetAnimation,
  widthSize,
  heighSize,
  path,
) {
  return Container(
    height: heighSize,
    child: Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
          child: Expanded(
            flex: 7,
            child: SlideTransition(
              position: _leftOffsetAnimation,
              child: Padding(
                padding: const EdgeInsets.all(30.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    buildTitleTextArt2(
                      path,
                      "ACOMPANHAMENTO E MONITORAMENTO DE EXPEDIÇÃO.",
                      widthSize,
                      TextAlign.left,
                    ),
                    buildTextArt2(
                      path,
                      "INSPEÇÃO VISUAL EXTERNA DA SUPERFÍCIE.",
                      widthSize,
                      TextAlign.left,
                    ),
                    buildTextArt2(
                      path,
                      "INSPEÇÃO DURANTE EMBARQUES EM PORTOS E CARREGAMENTO EM BASES",
                      widthSize,
                      TextAlign.left,
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
        Expanded(
          flex: 3,
          child: SlideTransition(
            position: _rightOffsetAnimation,
            child: Container(
              alignment: Alignment.centerRight,
              child: FractionallySizedBox(
                widthFactor: 1,
                child: Image.asset(path("Img/Imagem-02.png")),
              ),
            ),
          ),
        ),
      ],
    ),
  );
}
