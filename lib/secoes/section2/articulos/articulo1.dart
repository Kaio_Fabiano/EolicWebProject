import 'package:flutter/cupertino.dart';
import 'package:web_aplication/widgets/desktop/buildTextArt2.dart';
import 'package:web_aplication/widgets/desktop/buildTitleTextArt2.dart';

Container article1(
  _leftOffsetAnimation,
  _rightOffsetAnimation,
  widthSize,
  heighSize,
  path,
) {
  return Container(
    height: heighSize,
    child: Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Expanded(
          flex: 3,
          child: SlideTransition(
            position: _leftOffsetAnimation,
            child: Container(
              alignment: Alignment.centerLeft,
              child: FractionallySizedBox(
                widthFactor: 1,
                child: Image.asset(path("Img/Imagem-01.png")),
              ),
            ),
          ),
        ),
        Expanded(
          flex: 7,
          child: SlideTransition(
            position: _rightOffsetAnimation,
            child: Padding(
              padding: const EdgeInsets.all(30.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  buildTitleTextArt2(
                    path,
                    "INSPEÇÃO DE PÁS\.",
                    widthSize,
                    TextAlign.right,
                  ),
                  buildTextArt2(
                    path,
                    "AUDITORIA E INSPEÇÃO PARA VALIDAÇÃO DE PÁS NA BASE DE FABRICAÇÃO/\nACOMPANHAMENTO DO PROCESSO DE FABRICAÇÃO COMPLETO.",
                    widthSize,
                    TextAlign.right,
                  ),
                  buildTextArt2(
                    path,
                    "AUDITORIA E INSPEÇÃO PARA ACOMPANHAMENTO E EXECUÇÃO DE REPAROS E RETRABALHOS COMPLEXOS.",
                    widthSize,
                    TextAlign.right,
                  ),
                  buildTextArt2(
                    path,
                    "AUDITORIA E INSPEÇÃO EM PROCESSOS DE ULTRASOM.",
                    widthSize,
                    TextAlign.right,
                  ),
                  buildTextArt2(
                    path,
                    "AUDITORIA E INSPEÇÃO EM PROCESSOS DE ACABAMENTO E LIBERAÇÃO.",
                    widthSize,
                    TextAlign.right,
                  ),
                  buildTextArt2(
                    path,
                    "INSPEÇÃO INTERNA COM AUXILIO DE HOVER CÂMERA",
                    widthSize,
                    TextAlign.right,
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    ),
  );
}
