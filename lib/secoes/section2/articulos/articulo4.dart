import 'package:flutter/material.dart';
import 'package:web_aplication/widgets/desktop/buildTextArt2.dart';
import 'package:web_aplication/widgets/desktop/buildTitleTextArt2.dart';

Container article4(
  _leftOffsetAnimation,
  _rightOffsetAnimation,
  widthSize,
  heighSize,
  path,
) {
  return Container(
    height: heighSize,
    child: Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
          child: Expanded(
            flex: 5,
            child: SlideTransition(
              position: _leftOffsetAnimation,
              child: Padding(
                padding: const EdgeInsets.all(30.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    buildTitleTextArt2(
                      path,
                      "INSPEÇÃO PÓS MONTAGEM.",
                      widthSize,
                      TextAlign.right,
                    ),
                    buildTextArt2(
                      path,
                      "INSPEÇÃO VISUAL AUXILIO DE DRONES.",
                      widthSize,
                      TextAlign.right,
                    ),
                    buildTextArt2(
                      path,
                      "INSPEÇÃO VISUAL COM AUXILIO DE CORDAS.",
                      widthSize,
                      TextAlign.right,
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
        Expanded(
          flex: 6,
          child: SlideTransition(
            position: _rightOffsetAnimation,
            child: Container(
              alignment: Alignment.centerLeft,
              child: FractionallySizedBox(
                widthFactor: 1,
                child: Image.asset(path("Img/ImagemD-04.png")),
              ),
            ),
          ),
        ),
      ],
    ),
  );
}
