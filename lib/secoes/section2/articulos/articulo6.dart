import 'package:flutter/material.dart';
import 'package:web_aplication/widgets/desktop/buildTextArt2.dart';
import 'package:web_aplication/widgets/desktop/buildTitleTextArt2.dart';

Container article6(
  _leftOffsetAnimation,
  _rightOffsetAnimation,
  widthSize,
  heighSize,
  path,
) {
  return Container(
    height: heighSize,
    child: Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
          child: Expanded(
            flex: 5,
            child: SlideTransition(
              position: _leftOffsetAnimation,
              child: Padding(
                padding: const EdgeInsets.all(30.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    buildTitleTextArt2(
                      path,
                      "INSPEÇÃO FINAL DE GARANTIA.",
                      widthSize,
                      TextAlign.left,
                    ),
                    buildTextArt2(
                      path,
                      "MAPEAMENTO DOS CRITÉRIOS DE QUALIDADE ESTABELECIDOS EM CONTRATOS.",
                      widthSize,
                      TextAlign.left,
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
        Expanded(
          flex: 6,
          child: SlideTransition(
            position: _rightOffsetAnimation,
            child: Container(
              alignment: Alignment.centerRight,
              child: FractionallySizedBox(
                widthFactor: 1,
                child: Image.asset(path("Img/ImagemDN-06.png")),
              ),
            ),
          ),
        ),
      ],
    ),
  );
}
