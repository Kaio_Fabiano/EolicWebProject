import 'package:flutter/material.dart';

import 'articulosMobile/articulo1Mobile.dart';
import 'articulosMobile/articulo2Mobile.dart';
import 'articulosMobile/articulo3Mobile.dart';
import 'articulosMobile/articulo4Mobile.dart';
import 'articulosMobile/articulo5Mobile.dart';
import 'articulosMobile/articulo6Mobile.dart';

buildArticle2Mobile(
  List<Animation<Offset>> _leftOffsetAnimation,
  List<Animation<Offset>> _rightOffsetAnimation,
  widthSize,
  heighSize,
  article2HeighSizes,
  path,
) {
  return Container(
    child: Column(
      children: [
        Text(
          "NOSSOS SERVIÇOS",
          style: TextStyle(
            color: Color(0xff06C8BE),
            fontSize: 26,
            fontWeight: FontWeight.bold,
            fontFamily: "MazzardH",
          ),
        ),
        SizedBox(
          height: 50,
        ),
        article1Mobile(
          widthSize,
          article2HeighSizes[0] * 3.8,
          path,
        ),
        article2Mobile(
          widthSize,
          article2HeighSizes[1] * 2.5,
          path,
        ),
        article3Mobile(
          widthSize,
          article2HeighSizes[2] * 3,
          path,
        ),
        article4Mobile(
          widthSize,
          article2HeighSizes[3],
          path,
        ),
        article5Mobile(
          widthSize,
          article2HeighSizes[4],
          path,
        ),
        article6Mobile(
          widthSize,
          article2HeighSizes[5],
          path,
        ),
      ],
    ),
  );
}
