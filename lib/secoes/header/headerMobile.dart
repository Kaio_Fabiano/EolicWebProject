import 'package:flutter/material.dart';

buildHeaderMobile(
  rotateAnimation,
  headerHeighSize,
  heighSize,
  widthSize,
  bemVindoSize,
  path,
) {
  return Container(
    color: Colors.white,
    height: headerHeighSize * 1.2,
    width: widthSize,
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Container(
          alignment: Alignment.topCenter,
          child: SizedBox(
            width: bemVindoSize,
            height: bemVindoSize * .2,
            child: Image.asset(
              path("Img/bem-vindo.png"),
              alignment: Alignment.topCenter,
            ),
          ),
        ),
        SizedBox(
          height: widthSize * .23,
        ),
        SizedBox(
          height: headerHeighSize * .8,
          width: widthSize,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                alignment: Alignment.centerRight,
                child: Transform.rotate(
                  angle: rotateAnimation.value,
                  child: Image.asset(
                    path('Img/Catavento.png'),
                    width: widthSize * .4,
                    height: heighSize * .4,
                    alignment: Alignment.center,
                  ),
                ),
              ),
              Container(
                //color: Colors.blue,
                alignment: Alignment.center,
                child: SizedBox(
                  width: widthSize * .48,
                  height: heighSize * .48,
                  child: Image.asset(
                    path("Img/Logo-nome.png"),
                    alignment: Alignment.center,
                  ),
                ),
              ),
            ],
          ),
        ),
        // Container(
        //   child: VideoPlayer(_videoPlayerController),
        // )
      ],
    ),
  );
}
