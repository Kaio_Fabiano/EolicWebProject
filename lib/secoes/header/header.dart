import 'package:flutter/material.dart';

buildHeader(
  rotateAnimation,
  headerHeighSize,
  heighSize,
  widthSize,
  bemVindoSize,
  path,
) {
  return Container(
    color: Colors.white,
    height: headerHeighSize * 1.2,
    width: widthSize,
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Container(
          alignment: Alignment.topCenter,
          child: SizedBox(
            width: bemVindoSize,
            height: bemVindoSize * .2,
            child: Image.asset(
              path("Img/bem-vindo.png"),
              alignment: Alignment.topCenter,
            ),
          ),
        ),
        SizedBox(
          height: widthSize * .03,
        ),
        Center(
          child: SizedBox(
            height: headerHeighSize * .8,
            width: widthSize * .8,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  width: widthSize * .26,
                  height: heighSize * .7,
                  // color: Colors.black,
                  alignment: Alignment.centerRight,
                  child: Transform.rotate(
                    angle: rotateAnimation.value,
                    child: Image.asset(
                      path('Img/Catavento.png'),
                      width: widthSize * .6,
                      height: heighSize * .7,
                      alignment: Alignment.center,
                    ),
                  ),
                ),
                Container(
                  //color: Colors.blue,
                  alignment: Alignment.center,
                  child: SizedBox(
                    width: widthSize * .4,
                    height: heighSize * .4,
                    child: Image.asset(
                      path("Img/Logo-nome.png"),
                      alignment: Alignment.center,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        // Container(
        //   child: VideoPlayer(_videoPlayerController),
        // )
      ],
    ),
  );
}
