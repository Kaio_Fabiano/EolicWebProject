import 'package:flutter/material.dart';
import 'package:web_aplication/widgets/mobile/mobileArticle1Text.dart';
import 'package:web_aplication/widgets/mobile/mobileBuildTitlePlace.dart';

buildArticle1Mobile(
  fadeAnimation,
  widthSize,
  heighSize,
  article1HeighSize,
  path,
) {
  return Container(
    padding: EdgeInsets.only(
      left: 50,
      right: 50,
    ),
    height: article1HeighSize * .7,
    width: widthSize,
    decoration: BoxDecoration(
      color: Colors.white,
      image: DecorationImage(
        image: AssetImage(path("Img/Detalhe-1.png")),
        fit: BoxFit.fill,
      ),
    ),
    child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SizedBox(
          height: widthSize * .3,
        ),
        mobileBuildTitlePlace(
          "SOBRE A MEGAWIND SERVICE",
          widthSize,
          path,
        ),
        SizedBox(
          height: widthSize * .06,
        ),
        Expanded(
          child: mobileArticle1Text(
              widthSize,
              "UMA EMPRESA FORMADA POR PROFISSIONAIS COM LONGA EXPERIÊNCIA NO MERCADO EÓLICO NOS MAIS DIVERSOS SEGMENTOS, DESDE A FABRICAÇÃO DE COMPONENTES ATÉ A INSPEÇÃO DO FIM DE GARANTIA. NOSSA EQUIPE TEM COMO OBJETIVO ATENDER AS NECESSIDADES DE NOSSOS CLIENTES COM O MAIS ALTO NÍVEL DE COMPETÊNCIA TÉCNICA, INOVAÇÃO E FLEXIBILIDADE\. NOSSOS PILARES SÃO TRANSPARÊNCIA, CUIDADO COM AS PESSOAS, SEGURANÇA,QUALIDADE E PONTUALIDADE.",
              path),
        ),
        mobileBuildTitlePlace(
          "MISSÃO",
          widthSize,
          path,
        ),
        SizedBox(
          height: widthSize * .06,
        ),
        Expanded(
          child: mobileArticle1Text(
            widthSize,
            "SER UMA EMPRESA REFERÊNCIA EM PRESTAÇÃO DE SERVIÇOS E FABRICAÇÃO DE COMPONENTES EÓLICOS, CONTRIBUINDO DE FORMA ATIVA PARA O BEM ESTAR DA SOCIEDADE, OFERECENDO SOLUÇÕES INOVADORAS PARAGERAÇÃO DE ENERGIA LIMPA E RENOVÁVEL\.",
            path,
          ),
        ),
      ],
    ),
  );
}
