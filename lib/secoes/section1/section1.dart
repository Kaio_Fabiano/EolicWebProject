import 'package:flutter/material.dart';
import 'package:web_aplication/widgets/desktop/article1Text.dart';
import 'package:web_aplication/widgets/desktop/buildTitlePlace.dart';

buildArticle1(
  fadeAnimation,
  widthSize,
  heighSize,
  article1HeighSize,
  path,
) {
  return Container(
    padding: EdgeInsets.only(
      left: 50,
      right: 50,
    ),
    alignment: Alignment.center,
    height: article1HeighSize,
    width: widthSize,
    decoration: BoxDecoration(
      color: Colors.white,
      image: DecorationImage(
        image: AssetImage(path("Img/Detalhe-1.png")),
        fit: BoxFit.fill,
      ),
    ),
    child: FadeTransition(
      opacity: fadeAnimation,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(
            height: article1HeighSize * 0.2,
          ),
          buildTitlePlace(
            "SOBRE A MEGAWIND SERVICE",
            widthSize,
            path,
          ),
          SizedBox(
            height: heighSize * .03,
          ),
          Expanded(
            child: article1Text(
                widthSize,
                "UMA EMPRESA FORMADA POR PROFISSIONAIS COM LONGA EXPERIÊNCIA NO MERCADO EÓLICO NOS MAIS DIVERSOS SEGMENTOS, DESDE A FABRICAÇÃO DE COMPONENTES ATÉ A INSPEÇÃO DO FIM DE GARANTIA. NOSSA EQUIPE TEM COMO OBJETIVO ATENDER AS NECESSIDADES DE NOSSOS CLIENTES COM O MAIS ALTO NÍVEL DE COMPETÊNCIA TÉCNICA, INOVAÇÃO E FLEXIBILIDADE\. NOSSOS PILARES SÃO TRANSPARÊNCIA, CUIDADO COM AS PESSOAS, SEGURANÇA,QUALIDADE E PONTUALIDADE.",
                path),
          ),
          buildTitlePlace(
            "MISSÃO",
            widthSize,
            path,
          ),
          SizedBox(
            height: heighSize * .03,
          ),
          Expanded(
            child: article1Text(
                widthSize,
                "SER UMA EMPRESA REFERÊNCIA EM PRESTAÇÃO DE SERVIÇOS E FABRICAÇÃO DE COMPONENTES EÓLICOS, CONTRIBUINDO DE FORMA ATIVA PARA O BEM ESTAR DA SOCIEDADE, OFERECENDO SOLUÇÕES INOVADORAS PARAGERAÇÃO DE ENERGIA LIMPA E RENOVÁVEL\.",
                path),
          ),
        ],
      ),
    ),
  );
}
