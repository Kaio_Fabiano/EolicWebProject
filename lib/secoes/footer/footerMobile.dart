import 'package:flutter/material.dart';
import 'package:web_aplication/widgets/desktop/buildFooterIcon.dart';

import 'package:web_aplication/widgets/mobile/mobileBuildFooterTitlePlace.dart';

buildFooterMobile(
  widthSize,
  heighSize,
  path,
) {
  return Container(
    padding: EdgeInsets.only(top: 60),
    decoration: BoxDecoration(
      color: Colors.white,
      image: DecorationImage(
        image: AssetImage(
          path("Img/Detalhe-rodape.png"),
        ),
        fit: BoxFit.fill,
      ),
    ),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        Expanded(
          flex: 6,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              buildFooterTitlePlaceMobile(
                'COMO VOCÊ PREFERE FALAR COM A GENTE?',
                widthSize,
                path,
              ),
              SizedBox(
                height: 20,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 5, bottom: 20),
                child: Row(
                  children: [
                    buildFooterIcon(
                      path("Icons/Icon-WhatsApp.png"),
                      widthSize * 1.5,
                      "https://api.whatsapp.com/send?phone=5585987295236",
                    ),
                    SizedBox(
                      width: widthSize * .02,
                    ),
                    buildFooterIcon(
                      path("Icons/Icon-Localizacao.png"),
                      widthSize * 1.5,
                      "https://goo.gl/maps/CkmA4MeJsuytLAwr7",
                    ),
                    SizedBox(
                      width: widthSize * .02,
                    ),
                    buildFooterIcon(
                      path("Icons/Icon-E-mail.png"),
                      widthSize * 1.5,
                      "mailto:comercial@megawind.com.br",
                    ),
                    SizedBox(
                      width: widthSize * .02,
                    ),
                    buildFooterIcon(
                      path("Icons/Icon-Linkedin.png"),
                      widthSize * 1.5,
                      "https://www.linkedin.com/in/mega-wind-brazil-886113217/?lipi=urn%3Ali%3Apage%3Ad_flagship3_feed%3B%2BOD2uQ7SRvKbLRcgR8C29g%3D%3D",
                    ),
                    SizedBox(
                      width: widthSize * .02,
                    ),
                    buildFooterIcon(
                      path("Icons/Icon-Instagram.png"),
                      widthSize * 1.5,
                      "https://instagram.com/megawindbrazil?utm_medium=copy_link",
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        Expanded(
          flex: 2,
          child: FractionallySizedBox(
            widthFactor: 1,
            child: Image.asset(
              path("Img/Catavento-rodape.png"),
            ),
          ),
        )
      ],
    ),
  );
}
