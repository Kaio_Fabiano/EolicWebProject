import 'package:flutter/material.dart';
import 'package:web_aplication/widgets/desktop/buildFooterIcon.dart';
import 'package:web_aplication/widgets/desktop/buildFooterTitlePlace.dart';
import 'package:web_aplication/widgets/desktop/buildTextArt3.dart';
import 'package:web_aplication/widgets/desktop/buildTitleTextArt3.dart';

buildFooter(
  widthSize,
  heighSize,
  path,
) {
  return Padding(
    padding: EdgeInsets.only(top: 20),
    child: FractionallySizedBox(
      widthFactor: 1,
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          image: DecorationImage(
            image: AssetImage(
              path("Img/Detalhe-rodape.png"),
            ),
            fit: BoxFit.fill,
          ),
        ),
        child: Padding(
          padding: const EdgeInsets.only(top: 100, right: 20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 30, right: 30),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      flex: 3,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          buildTitleTextArt3(
                            'REPAROS DE COMPONENTES EÓLICOS.',
                            widthSize,
                          ),
                          buildTextArt3(
                            'AVALIAÇÃO E INSPENSÃO RÁPIDA PARA REPAROS COM DISPOSIÇÕES PRÓPRIAS OU PRÉ-DEFINIDAS. \nRÁPIDA MOBILIZAÇÃO E RESPOSTA AO CLIENTE. \nDANOS POR DESCARGAS ATMOSFÉRICAS \nREPAROS POR DESGASTE E EROSÃO AO BORDO DE ATAQUE E AO BORDO E FULGA \nBALANCEAMENTO DE PÁS. \nREPAROS EM GRANDE ESCALA, COSMÉTICOS SUPERFICIAIS OU COMPLEXOS. \nDANOS ESTRUTURAIS EM ÁREAS DE COLAGEM, FALHAS E DESGASTES.',
                            widthSize,
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      width: widthSize * .02,
                    ),
                    Expanded(
                      flex: 3,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          buildTitleTextArt3(
                            'SERVIÇOS EM PARQUES EÓLICOS.',
                            widthSize,
                          ),
                          buildTextArt3(
                            'LAVAGEM DE PÁS PRÉ-MONTAGEM.\n RECUPERAÇÃO DE EMBALAGENS. \nCONFECÇÃO E DESENVOLVIMENTO DE MATERIAIS COMPÓSITOS E MECÂNICOS \nDE TRANSPORTE E ARMAZEMANAMENTO DE PÁS',
                            widthSize,
                          ),
                          buildTitleTextArt3(
                            '\nMANUTENÇÃO EM TORRES.',
                            widthSize,
                          ),
                          buildTextArt3(
                            'INSPEÇÃO DE SUPERFÍCIE, SOLDA E PINTURA. \nPINTURA',
                            widthSize,
                          ),
                        ],
                      ),
                    ),
                    Expanded(
                      flex: 3,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          buildTitleTextArt3(
                            'SERVIÇOS EM PARQUES EÓLICOS.',
                            widthSize,
                          ),
                          buildTextArt3(
                            'LAVAGEM DE PÁS PRÉ-MONTAGEM.\nRECUPERAÇÃO DE EMBALAGENS. \nCONFECÇÃO E DESENVOLVIMENTO DE MATERIAIS COMPÓSITOS E MECÂNICOS \nDE TRANSPORTE E ARMAZEMANAMENTO DE PÁS',
                            widthSize,
                          ),
                          buildTitleTextArt3(
                            '\nFABRICAÇÃO.',
                            widthSize,
                          ),
                          buildTextArt3(
                            'FABRICAÇÃO DE COMPONENTES EÓLICOS.',
                            widthSize,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              FractionallySizedBox(
                widthFactor: 1,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Expanded(
                      flex: 5,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          buildFooterTitlePlace(
                            'COMO VOCÊ PREFERE FALAR COM A GENTE?',
                            widthSize,
                            path,
                          ),
                          SizedBox(
                            height: 50,
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 30),
                            child: Row(
                              children: [
                                SizedBox(
                                  width: 10,
                                ),
                                buildFooterIcon(
                                    path("Icons/Icon-WhatsApp.png"),
                                    widthSize,
                                    "https://api.whatsapp.com/send?phone=5585987295236"),
                                SizedBox(
                                  width: widthSize * .02,
                                ),
                                buildFooterIcon(
                                    path("Icons/Icon-Localizacao.png"),
                                    widthSize,
                                    "https://goo.gl/maps/CkmA4MeJsuytLAwr7"),
                                SizedBox(
                                  width: widthSize * .02,
                                ),
                                buildFooterIcon(
                                    path("Icons/Icon-E-mail.png"),
                                    widthSize,
                                    "mailto:comercial@megawind.com.br"),
                                SizedBox(
                                  width: widthSize * .02,
                                ),
                                buildFooterIcon(
                                    path("Icons/Icon-Linkedin.png"),
                                    widthSize,
                                    "https://www.linkedin.com/in/mega-wind-brazil-886113217/?lipi=urn%3Ali%3Apage%3Ad_flagship3_feed%3B%2BOD2uQ7SRvKbLRcgR8C29g%3D%3D"),
                                SizedBox(
                                  width: widthSize * .02,
                                ),
                                buildFooterIcon(
                                  path("Icons/Icon-Instagram.png"),
                                  widthSize,
                                  "https://instagram.com/megawindbrazil?utm_medium=copy_link",
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    Expanded(
                      flex: 2,
                      child: FractionallySizedBox(
                        widthFactor: 1,
                        child: Image.asset(
                          path("Img/Catavento-rodape.png"),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    ),
  );
}
