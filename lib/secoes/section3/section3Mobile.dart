import 'package:flutter/material.dart';
import 'package:web_aplication/widgets/mobile/mobileBuildTextArt3.dart';
import 'package:web_aplication/widgets/mobile/mobileBuildTitleTextArt3.dart';

buildArticle3Mobile(
  fadeAnimation,
  widthSize,
  heighSize,
  article1HeighSize,
  path,
) {
  return Container(
    padding: EdgeInsets.all(30),
    child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          "MAIS SERVIÇOS",
          style: TextStyle(
            color: Color(0xff06C8BE),
            fontSize: 26,
            fontWeight: FontWeight.bold,
            fontFamily: "MazzardH",
          ),
        ),
        buildTitleTextArt3Mobile(
          "\nMANUTENÇÃO EM TORRES.\n",
        ),
        buildTextArt3Mobile(
          'INSPEÇÃO DE SUPERFÍCIE, SOLDA E PINTURA. \n',
        ),
        buildTitleTextArt3Mobile(
          '\nSERVIÇOS EM PARQUES EÓLICOS. \n',
        ),
        buildTextArt3Mobile(
          'LAVAGEM DE PÁS PRÉ-MONTAGEM.\nRECUPERAÇÃO DE EMBALAGENS. \nCONFECÇÃO E DESENVOLVIMENTO DE MATERIAIS COMPÓSITOS E MECÂNICOS \nDE TRANSPORTE E ARMAZEMANAMENTO DE PÁS\n\n',
        ),
        buildTitleTextArt3Mobile(
          'SERVIÇOS EM PARQUES EÓLICOS.\n',
        ),
        buildTextArt3Mobile(
          'LAVAGEM DE PÁS PRÉ-MONTAGEM.\n RECUPERAÇÃO DE EMBALAGENS. \nCONFECÇÃO E DESENVOLVIMENTO DE MATERIAIS COMPÓSITOS E MECÂNICOS \nDE TRANSPORTE E ARMAZEMANAMENTO DE PÁS.\n',
        ),
        buildTitleTextArt3Mobile(
          '\n\nFABRICAÇÃO.\n',
        ),
        buildTextArt3Mobile(
          'FABRICAÇÃO DE COMPONENTES EÓLICOS.\n',
        ),
        buildTitleTextArt3Mobile(
          'REPAROS DE COMPONENTES EÓLICOS,\n',
        ),
        buildTextArt3Mobile(
          'AVALIAÇÃO E INSPENSÃO RÁPIDA PARA REPAROS COM DISPOSIÇÕES PRÓPRIAS OU PRÉ-DEFINIDAS. \nRÁPIDA MOBILIZAÇÃO E RESPOSTA AO CLIENTE. \nDANOS POR DESCARGAS ATMOSFÉRICAS \nREPAROS POR DESGASTE E EROSÃO AO BORDO DE ATAQUE E AO BORDO E FULGA \nBALANCEAMENTO DE PÁS. \nREPAROS EM GRANDE ESCALA, COSMÉTICOS SUPERFICIAIS OU COMPLEXOS. \nDANOS ESTRUTURAIS EM ÁREAS DE COLAGEM, FALHAS E DESGASTES.',
        ),
      ],
    ),
  );
}
