import 'package:flutter/material.dart';

buildTextArt3(
  String texto,
  double fontSizes,
) {
  return Text(
    texto,
    textAlign: TextAlign.left,
    style: new TextStyle(
      fontSize: fontSizes * .008,
      fontFamily: "MazzardH",
      color: Colors.white,
    ),
  );
}
