import 'package:flutter/material.dart';

import 'dart:html' as html;

buildFooterIcon(assetIcon, widthSize, url) {
  return GestureDetector(
    onTap: () {
      html.window.open(url, 'new tab');
    },
    child: Container(
      width: widthSize * .08,
      child: Image.asset(
        assetIcon,
      ),
    ),
  );
}
