import 'package:flutter/material.dart';

Text article1Text(
  widthSize,
  text,
  path,
) {
  return Text(
    text,
    textAlign: TextAlign.justify,
    style: TextStyle(
      color: Colors.white,
      fontSize: widthSize * .018,
      fontWeight: FontWeight.w500,
      fontFamily: "MazzardH",
    ),
  );
}
