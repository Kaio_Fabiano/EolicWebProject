import 'package:flutter/material.dart';
import 'buildTitle.dart';

Container buildTitlePlace(
  title,
  widthSize,
  path,
) {
  return Container(
    padding: EdgeInsets.only(
      left: 15,
      right: 15,
      top: 5,
      bottom: 5,
    ),
    decoration: new BoxDecoration(
      borderRadius: BorderRadius.circular(50),
      color: Colors.white,
      boxShadow: [
        BoxShadow(
          color: Colors.black26,
          blurRadius: 9, // soften the shadow
          spreadRadius: 1, //extend the shadow
        )
      ],
    ),
    child: buildTitle(
      title,
      widthSize,
      path,
    ),
  );
}
