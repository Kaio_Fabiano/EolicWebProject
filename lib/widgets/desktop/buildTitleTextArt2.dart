import 'package:flutter/material.dart';

buildTitleTextArt2(
  path,
  String texto,
  double fontSizes,
  textAlign,
) {
  return Text(
    texto,
    textAlign: textAlign,
    style: new TextStyle(
      fontSize: fontSizes * .026,
      fontFamily: "MazzardH",
      color: Color(0xff06C8BE),
    ),
  );
}
