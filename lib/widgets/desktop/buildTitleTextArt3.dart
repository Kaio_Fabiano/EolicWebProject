import 'package:flutter/material.dart';

buildTitleTextArt3(
  String texto,
  double fontSizes,
) {
  return Text(
    texto,
    textAlign: TextAlign.left,
    style: new TextStyle(
      fontSize: fontSizes * .014,
      fontFamily: "MazzardH",
      color: Colors.white,
    ),
  );
}
