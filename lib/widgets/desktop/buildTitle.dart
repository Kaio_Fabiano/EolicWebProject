import 'package:flutter/material.dart';

Text buildTitle(
  text,
  widthSize,
  path,
) {
  return Text(
    text,
    textAlign: TextAlign.center,
    style: TextStyle(
      fontSize: widthSize * .025,
      fontWeight: FontWeight.bold,
      color: Color(0xff06C8BE),
      fontFamily: "MazzardH",
    ),
    maxLines: 1,
  );
}
