import 'package:flutter/material.dart';

import 'buildTitle.dart';

Container buildFooterTitlePlace(
  title,
  widthSize,
  path,
) {
  return Container(
    padding: EdgeInsets.only(top: 8, bottom: 7, right: 15, left: 30),
    decoration: new BoxDecoration(
      borderRadius: BorderRadius.only(
        bottomRight: Radius.circular(30),
        topRight: Radius.circular(30),
      ),
      color: Colors.white,
      boxShadow: [
        BoxShadow(
          color: Colors.black26,
          blurRadius: 9, // soften the shadow
          spreadRadius: 1, //extend the shadow
        )
      ],
    ),
    child: buildTitle(
      title,
      widthSize,
      path,
    ),
  );
}
