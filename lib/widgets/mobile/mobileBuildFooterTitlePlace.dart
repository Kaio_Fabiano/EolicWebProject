import 'package:flutter/material.dart';

Container buildFooterTitlePlaceMobile(
  title,
  widthSize,
  path,
) {
  return Container(
    padding: EdgeInsets.only(
      top: 5,
      bottom: 5,
      right: 15,
      left: 5,
    ),
    decoration: new BoxDecoration(
      borderRadius: BorderRadius.only(
        bottomRight: Radius.circular(30),
        topRight: Radius.circular(30),
      ),
      color: Colors.white,
      boxShadow: [
        BoxShadow(
          color: Colors.black26,
          blurRadius: 9, // soften the shadow
          spreadRadius: 1, //extend the shadow
        )
      ],
    ),
    child: Text(
      title,
      textAlign: TextAlign.left,
      style: TextStyle(
        fontSize: 11,
        color: Color(0xff06C8BE),
        fontWeight: FontWeight.bold,
        fontFamily: "MazzardH",
      ),
      maxLines: 1,
    ),
  );
}
