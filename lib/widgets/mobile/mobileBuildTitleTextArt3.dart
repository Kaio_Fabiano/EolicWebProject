import 'package:flutter/material.dart';

buildTitleTextArt3Mobile(
  String texto,
) {
  return Text(
    texto,
    textAlign: TextAlign.justify,
    style: new TextStyle(
      fontSize: 11,
      fontFamily: "MazzardH",
      color: Color(0xff06C8BE),
    ),
  );
}
