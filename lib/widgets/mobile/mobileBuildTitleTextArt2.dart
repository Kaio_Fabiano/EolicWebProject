import 'package:flutter/material.dart';

buildTitleTextArt2Mobile(
  path,
  String texto,
  double fontSizes,
  textAlign,
) {
  return Text(
    texto,
    textAlign: textAlign,
    style: new TextStyle(
      fontSize: 12,
      fontFamily: "MazzardH",
      color: Color(0xff06C8BE),
    ),
  );
}
