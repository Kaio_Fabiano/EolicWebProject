import 'package:flutter/material.dart';
import 'package:web_aplication/widgets/mobile/mobileBuildTitle.dart';

import 'mobileBuildTitle.dart';

Container mobileBuildTitlePlace(
  title,
  widthSize,
  path,
) {
  return Container(
    padding: EdgeInsets.only(
      left: 15,
      right: 15,
      top: 5,
      bottom: 5,
    ),
    decoration: new BoxDecoration(
      borderRadius: BorderRadius.circular(50),
      color: Colors.white,
      boxShadow: [
        BoxShadow(
          color: Colors.black26,
          blurRadius: 9, // soften the shadow
          spreadRadius: 1, //extend the shadow
        )
      ],
    ),
    child: buildTitleMobile(
      title,
      widthSize,
      path,
    ),
  );
}
