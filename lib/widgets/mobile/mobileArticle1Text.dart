import 'package:flutter/material.dart';

Text mobileArticle1Text(
  widthSize,
  text,
  path,
) {
  return Text(
    text,
    textAlign: TextAlign.justify,
    style: TextStyle(
      color: Colors.white,
      fontWeight: FontWeight.w500,
      fontFamily: "MazzardH",
      fontSize: 11,
    ),
  );
}
