import 'package:flutter/material.dart';

buildTextArt2Mobile(
  path,
  String texto,
  double fontSizes,
  textAlign,
) {
  return Text(
    texto,
    textAlign: textAlign,
    style: new TextStyle(
      fontSize: 10,
      fontFamily: "MazzardH",
      color: Color(0xff06C8BE),
    ),
  );
}
