import 'package:flutter/material.dart';

Text buildTitleMobile(
  text,
  widthSize,
  path,
) {
  return Text(
    text,
    textAlign: TextAlign.center,
    style: TextStyle(
      fontSize: 22,
      color: Color(0xff06C8BE),
      fontWeight: FontWeight.bold,
      fontFamily: "MazzardH",
    ),
    maxLines: 1,
  );
}
