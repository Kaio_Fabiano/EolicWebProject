import 'package:flutter/material.dart';

buildTextArt3Mobile(
  String texto,
) {
  return Text(
    texto,
    textAlign: TextAlign.left,
    style: new TextStyle(
      fontSize: 8,
      fontFamily: "MazzardH",
      color: Color(0xff06C8BE),
    ),
  );
}
