import 'package:flutter/material.dart';
import 'package:web_aplication/secoes/footer/footer.dart';
import 'package:web_aplication/secoes/header/header.dart';
import 'package:web_aplication/secoes/section1/section1.dart';
import 'package:web_aplication/secoes/section2/section2.dart';

Container buildBody(
  backgroundColor,
  context,
  rotateAnimation,
  fadeAnimation,
  widthSize,
  heighSize,
  headerHeighSize,
  _scrollController,
  List<Animation<Offset>> _leftOffsetAnimation,
  List<Animation<Offset>> _rightOffsetAnimation,
  article1HeighSizes,
  article2HeighSizes,
  bemVindoSize,
  path,
) {
  return Container(
    child: ListView(
      controller: _scrollController,
      children: [
        Stack(
          children: [
            Column(children: [
              buildHeader(rotateAnimation, headerHeighSize, heighSize,
                  widthSize, bemVindoSize * 8, path),
              buildArticle1(
                fadeAnimation,
                widthSize,
                heighSize,
                article1HeighSizes,
                path,
              ),
            ]),
          ],
        ),
        buildArticle2(
          _leftOffsetAnimation,
          _rightOffsetAnimation,
          widthSize,
          heighSize,
          article2HeighSizes,
          path,
        ),
        buildFooter(
          widthSize,
          heighSize,
          path,
        ),
      ],
    ),
  );
}
