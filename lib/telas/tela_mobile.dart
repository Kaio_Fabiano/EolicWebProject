import 'package:flutter/material.dart';
import 'package:web_aplication/secoes/footer/footerMobile.dart';
import 'package:web_aplication/secoes/header/headerMobile.dart';
import 'package:web_aplication/secoes/section1/section1Mobile.dart';
import 'package:web_aplication/secoes/section2/section2Mobile.dart';
import 'package:web_aplication/secoes/section3/section3Mobile.dart';

Container buildMobileBody(
  backgroundColor,
  context,
  rotateAnimation,
  fadeAnimation,
  widthSize,
  heighSize,
  headerHeighSize,
  _scrollController,
  List<Animation<Offset>> _leftOffsetAnimation,
  List<Animation<Offset>> _rightOffsetAnimation,
  article1HeighSizes,
  article2HeighSizes,
  bemVindoSize,
  path,
) {
  return Container(
    child: ListView(
      controller: _scrollController,
      children: [
        buildHeaderMobile(rotateAnimation, headerHeighSize * 3.5, heighSize,
            widthSize, bemVindoSize * 15, path),
        buildArticle1Mobile(
          fadeAnimation,
          widthSize,
          heighSize,
          article1HeighSizes * 4,
          path,
        ),
        buildArticle2Mobile(
          _leftOffsetAnimation,
          _rightOffsetAnimation,
          widthSize,
          heighSize * 4,
          article2HeighSizes,
          path,
        ),
        buildArticle3Mobile(
          fadeAnimation,
          widthSize,
          heighSize,
          article1HeighSizes * 4,
          path,
        ),
        buildFooterMobile(
          widthSize,
          heighSize,
          path,
        ),
      ],
    ),
  );
}
